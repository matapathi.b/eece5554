# Assignment PA1 by Bavesh M.
The aim is to make 4 nodes: a Publisher-Subscriber pair and a Client-Server pair.
Before running either do build your workspace using ```catkin_make```
Also do not forget to run source ```source ./devel/setup.bash``` to get the nodes working

## Running Publisher-Subscriber
1. Run ```roscore```
2. In a new terminal, run ```rosrun PA1 talker.py```
3. In an *another* terminal, run ```rosrun PA1 listener.py```

## Running Publisher-Subscriber
1. Run ```roscore```
2. In a new terminal, run ```rosrun PA1 add_two_ints_server.py```
3. In an *another* terminal, run ```rosrun PA1 add_two_ints_client.py``` followed by *2 int arguments*

