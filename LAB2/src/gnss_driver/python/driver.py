#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import rospy
import utm
import serial
import utm
import sys
from gnss_driver.msg import gnss_msg

if __name__ == '__main__':
    rospy.init_node('gnsssensor')

    args = rospy.myargv(argv=sys.argv)
    serial_port = rospy.get_param('~port',args[1])
    serial_baud = rospy.get_param('~baudrate',115200)
    sampling_rate = rospy.get_param('~sampling_rate',25.0)

    port = serial.Serial(serial_port, serial_baud, timeout=3)
    # rospy.lodgebug("Using GPS on port" + serial_port + )

    gps_pub = rospy.Publisher("gnss", gnss_msg)

    msg = gnss_msg()

    sleeptime = 1/sampling_rate - 0.025
    print("TRYING")
    try:
        while not rospy.is_shutdown():
            line = port.readline()
            if "$GNGGA" in str(line):
                data = str(line).split(',')
                print(data)
                if data[1][0:2] == '':
                    continue
                HH = int(data[1][0:2])
                MM = int(data[1][2:4])
                SS = int(data[1][4:6])
                ns = int(data[1][7:])*1000
                lat = float(data[2][:-10]) + float(data[2][-10:])/60
                lon = float(data[4][:-10]) + float(data[4][-10:])/60
                alt = float(data[9])
                quality = int(data[6])
                print(data[9])

                UTM = utm.from_latlon(lat, lon)
                print(HH, MM, SS)
                msg.header.stamp.secs = HH*3600+MM*60+SS
                msg.header.stamp.nsecs = ns
                msg.header.frame_id = 'GNSS1_Frame'

                msg.Latitude = lat
                msg.Longitude = lon
                msg.Altitude = alt
                msg.UTM_easting = UTM[0]
                msg.UTM_northing = UTM[1]
                msg.Zone = UTM[2]
                msg.Letter = UTM[3]
                msg.quality = quality
                gps_pub.publish(msg)
                print(str(msg))

            rospy.sleep(sleeptime)
    except rospy.ROSInterruptException:
        port.close()
    except serial.serialutil.SerialException:
        rospy.loginfo("Shutting down GPS node")




