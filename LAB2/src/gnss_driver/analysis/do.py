import bagpy
from bagpy import bagreader
import pandas as pd

bag = bagreader('/home/bavesh/Documents/EECE5554/TempLAB2/src/gnss_driver/data/stationary_bad.bag')
data = bag.message_by_topic('/gnss')
readings = pd.read_csv(data)
readings['UTM_easting'] = readings['UTM_easting'] - readings['UTM_easting'].min()
readings['UTM_northing'] = readings['UTM_northing'] - readings['UTM_northing'].min()
print(readings[['UTM_easting', 'UTM_northing']])
print(readings)

readings.to_csv('/home/bavesh/Documents/EECE5554/TempLAB2/src/gnss_driver/data/stationary_bad.csv')
