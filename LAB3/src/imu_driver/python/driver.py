#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import rospy
import serial
import utm
import sys
import numpy as np
from imu_driver.msg import imu_msg

if __name__ == '__main__':
    rospy.init_node('imusensor')

    args = rospy.myargv(argv=sys.argv)
    serial_port   = rospy.get_param('~port',args[1])
    serial_baud   = rospy.get_param('~baudrate',115200)
    sampling_rate = rospy.get_param('~sampling_rate',40.0)

    port = serial.Serial(serial_port, serial_baud, timeout=3)

    imu_pub = rospy.Publisher("imu", imu_msg)

    msg = imu_msg()
    port.write(b"$VNWRG,07,40*xx")

    print("TRYING")
    try:
        while not rospy.is_shutdown():
            line = port.readline()
            if "$VNYMR" in str(line):
                data = str(line).split(',')
                print(data)
                if len(data) < 13:
                    continue
                try:
                    y  = float(data[1])*np.pi /180
                    p  = float(data[2])*np.pi /180
                    r  = float(data[3])*np.pi /180
                    mx = float(data[4])
                    my = float(data[5])
                    mz = float(data[6])
                    ax = float(data[7])
                    ay = float(data[8])
                    az = float(data[9])
                    gx = float(data[10])
                    gy = float(data[11])
                    gz = float(data[12][0:9])
                except:
                    continue

                now = rospy.get_rostime()
                msg.Header.stamp.secs = int(now.secs)
                msg.Header.stamp.nsecs = int(now.nsecs)
                msg.Header.frame_id = 'IMU1_Frame'

                qx = np.sin(r/2)*np.cos(p/2)*np.cos(y/2)-np.cos(r/2)*np.sin(p/2)*np.sin(y/2)
                qy = np.cos(r/2)*np.sin(p/2)*np.cos(y/2)+np.sin(r/2)*np.cos(p/2)*np.sin(y/2)
                qz = np.cos(r/2)*np.cos(p/2)*np.sin(y/2)-np.sin(r/2)*np.sin(p/2)*np.cos(y/2)
                qw = np.cos(r/2)*np.cos(p/2)*np.cos(y/2)+np.sin(r/2)*np.sin(p/2)*np.sin(y/2)

                msg.IMU.orientation.x         = qx
                msg.IMU.orientation.y         = qy
                msg.IMU.orientation.z         = qz
                msg.IMU.orientation.w         = qw
                msg.IMU.linear_acceleration.x = ax
                msg.IMU.linear_acceleration.y = ay
                msg.IMU.linear_acceleration.z = az
                msg.IMU.angular_velocity.x    = gx
                msg.IMU.angular_velocity.y    = gy
                msg.IMU.angular_velocity.z    = gz
                msg.MagField.magnetic_field.x      = mx
                msg.MagField.magnetic_field.y      = my
                msg.MagField.magnetic_field.z      = mz
                imu_pub.publish(msg)
                print(str(msg))

    except rospy.ROSInterruptException:
        port.close()
    except serial.serialutil.SerialException:
        rospy.loginfo("Shutting down IMU node")




