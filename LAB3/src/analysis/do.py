import bagpy
from bagpy import bagreader
import pandas as pd

bag = bagreader('/home/bavesh/Documents/EECE5554/LAB3/src/analysis/imu_good.bag')
data = bag.message_by_topic('/imu')
readings = pd.read_csv(data)
print(readings)

readings.to_csv('/home/bavesh/Documents/EECE5554/LAB3/src/analysis/imu.csv')